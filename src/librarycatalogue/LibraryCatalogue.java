package librarycatalogue;

import java.util.HashMap;
import java.util.Map;

public class LibraryCatalogue {

    Map<String, Book> bookCollection = new HashMap<>();

    int currentDay = 0;
    int checkOutPeriod = 7;
    double initalLateFee = 0.50;
    double perDayLateFee = 1.00;

    //Default Constructor
    public LibraryCatalogue(Map<String, Book> collection) {
        this.bookCollection = collection;
    }

    //Constructor
    public LibraryCatalogue(Map<String, Book> collection, int currentDay, int checkOutPeriod, double initalLateFee,
            double perDayLateFee) {
        this.bookCollection = collection;
        this.currentDay = currentDay;
        this.checkOutPeriod = checkOutPeriod;
        this.initalLateFee = initalLateFee;
        this.perDayLateFee = perDayLateFee;
    }

    //Getters
    public Map<String, Book> getCollection() {
        return this.bookCollection;
    }

    public int getCurrentDay() {
        return this.currentDay;
    }

    public int getCheckOutPeriod() {
        return this.checkOutPeriod;
    }

    public double getInitalLateFee() {
        return this.initalLateFee;
    }

    public double getPerDayLateFee() {
        return this.perDayLateFee;
    }

    public Book getBook(String bookTitle) {
        return getCollection().get(bookTitle);
    }

    //Setters
    public void nextDay() {
        currentDay++;
    }

    public void setDay(int day) {
        currentDay = day;
    }

    //Methods
    public void checkOutBook(String title) {
        Book book = getBook(title);

        if (book.getIsCheckedOut()) {
            sorryBookCheckedOut(book);
        } else {
            book.setIsCheckedOut(true, currentDay);
            System.out.println("You just checked out " + title + ". It is due on day " + (getCurrentDay()
                    + getCheckOutPeriod()) + ".");
        }
    }

    public void returnBook(String title) {

        Book book = getBook(title);
        int daysLate = currentDay - (book.getDayCheckedOut() + getCheckOutPeriod());
        if (daysLate > 0) {
            System.out.println("You owe the library $" + (getInitalLateFee() + (daysLate * getPerDayLateFee()))
                    + " because your book is " + daysLate + " days overdue.");
        } else {
            System.out.println("Book returned. Thank you!");
        }
        book.setIsCheckedOut(false, -1);

    }

    public void sorryBookCheckedOut(Book book) {
        System.out.println("Sorry, " + book.getTitle() + " was checked out for " + getCheckOutPeriod() + " days."
                + " It should be available on day " + (book.getDayCheckedOut() + getCheckOutPeriod()) + ".");

    }
}
