package librarycatalogue;

public class Book implements IBook {

    String title;
    int pageCount;
    int ISBN;
    boolean isCheckedOut;
    int dayCheckedOut;

    //Constructor
    public Book(String title, int pageCount, int ISBN, boolean isCheckedOut, int dayCheckedOut) {
        this.title = title;
        this.pageCount = pageCount;
        this.ISBN = ISBN;
        this.isCheckedOut = isCheckedOut;
        this.dayCheckedOut = dayCheckedOut;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public int getPageCount() {
        return this.pageCount;
    }

    @Override
    public int getISBN() {
        return this.ISBN;
    }

    @Override
    public boolean getIsCheckedOut() {
        return this.isCheckedOut;
    }

    @Override
    public int getDayCheckedOut() {
        return this.dayCheckedOut;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    @Override
    public void setIsCheckedOut(boolean isCheckedOut, int currentDayCheckedout) {
        this.isCheckedOut = isCheckedOut;
        setDayCheckedOut(currentDayCheckedout);
    }

    @Override
    public void setDayCheckedOut(int day) {
        this.dayCheckedOut = day;
    }

}
