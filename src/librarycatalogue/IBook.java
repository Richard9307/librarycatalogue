package librarycatalogue;

public interface IBook {

    String getTitle();

    int getPageCount();

    int getISBN();

    boolean getIsCheckedOut();

    int getDayCheckedOut();

    void setTitle(String title);

    void setPageCount(int pageCount);

    void setISBN(int ISBN);

    void setIsCheckedOut(boolean isCheckedOut, int currentDayCheckedout);

    void setDayCheckedOut(int day);

}
