package librarycatalogue;

import java.util.HashMap;
import java.util.Map;

public class Main {
    
    public static void main(String[] args) {
        
        Map<String, Book> bookCollection = new HashMap<>();
        Book harry = new Book("Harry Potter", 958, 6589785, false, -1);
        
        bookCollection.put("Harry Potter", harry);
        
        LibraryCatalogue lib = new LibraryCatalogue(bookCollection);
        lib.checkOutBook("Harry Potter");
        lib.nextDay();
        lib.nextDay();
        lib.checkOutBook("Harry Potter");
        lib.setDay(17);
        lib.returnBook("Harry Potter");
        lib.checkOutBook("Harry Potter");
    }
    
}
